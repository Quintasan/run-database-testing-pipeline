#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'gitlab'
  gem 'tty-logger'
  gem 'tty-prompt'
  gem 'tty-progressbar'
  gem 'json'
  gem 'tempfile'
  gem 'pry-byebug'
end

DATADIR = Pathname.new(ENV.fetch('XDG_DATA_HOME') { Pathname.new(Dir.home).join('.local', 'share') })
STATEFILE = DATADIR.join('run-db-testing-pipeline.json')
STATE = begin
  JSON.parse(File.read(STATEFILE))
rescue Errno::ENOENT
  {}
end

GITLAB_API_ENDPOINT = ENV.fetch('GITLAB_API_ENDPOINT', 'https://gitlab.com/api/v4')
GITLAB_API_PRIVATE_TOKEN = ENV.fetch('GITLAB_API_PRIVATE_TOKEN',
                                     `op item get "GitLab Personal Access Token" --fields label=credential`)
LOGGER = TTY::Logger.new do |config|
  config.level = :debug
end
PROMPT = TTY::Prompt.new
GITLAB_PROJECT_ID = 278_964
JOB_NAME = 'db:gitlabcom-database-testing'
CLIENT = Gitlab.client(
  endpoint: GITLAB_API_ENDPOINT,
  private_token: GITLAB_API_PRIVATE_TOKEN
)

def get_mr_id_from_url(url)
  parsed_url = URI.parse(url)
  path = parsed_url.path.split('/')
  index = path.index('merge_requests')
  path[index + 1]
end

def get_latest_pipeline(merge_request_id)
  LOGGER.info { "Getting latest pipeline for MR !#{merge_request_id}" }
  CLIENT.merge_request_pipelines(GITLAB_PROJECT_ID, merge_request_id).first.id
end

def pipeline_id_in_cache?(pipeline_id)
  return unless STATE.key?(pipeline_id.to_s)

  existing_job_id = STATE.fetch(pipeline_id.to_s)
  LOGGER.debug { "pipeline #{pipeline_id} found in the state file, job id is #{existing_job_id}" }
  existing_job_id
end

def get_pipeline_jobs(pipeline_id)
  LOGGER.debug { "latest pipeline id is #{pipeline_id}" }

  existing_job_id = pipeline_id_in_cache?(pipeline_id)
  if existing_job_id
    restart_database_testing_job(existing_job_id)
    return []
  end

  LOGGER.info { "Getting pipeline jobs for pipeline ##{pipeline_id}" }
  bar = TTY::ProgressBar.new('getting all jobs [:bar]', total: nil, width: 40)
  get_all_jobs = Thread.new { CLIENT.pipeline_jobs(GITLAB_PROJECT_ID, pipeline_id).auto_paginate }
  bar.advance until get_all_jobs.join(0.1)
  bar.finish
  get_all_jobs.value
end

def find_database_testing_pipeline_job(jobs)
  jobs.find { |job| job['name'] == 'db:gitlabcom-database-testing' }
end

def restart_database_testing_job(job_id)
  LOGGER.info 'Database testing pipeline was already started for this pipeline'

  should_restart = PROMPT.yes?('Do you want to restart the job?') do |q|
    q.default false
  end

  CLIENT.job_retry(GITLAB_PROJECT_ID, job_id) if should_restart
end

def start_database_testing_job(job)
  return unless job

  pipeline_id = job['pipeline']['id']
  job_id = job['id']

  LOGGER.info 'Starting database testing pipeline'
  LOGGER.debug { "pipeline id: #{pipeline_id}, job id: #{job_id}" }
  CLIENT.job_play(GITLAB_PROJECT_ID, job_id)
  STATE[pipeline_id] = job_id
end

begin
  tempfile = Tempfile.new('state')

  ARGV.each do |url|
    get_mr_id_from_url(url)
      .then { |merge_request_id| get_latest_pipeline(merge_request_id) }
      .then { |pipeline_id| get_pipeline_jobs(pipeline_id).compact }
      .then { |pipeline_jobs| find_database_testing_pipeline_job(pipeline_jobs) }
      .then { |database_testing_job| start_database_testing_job(database_testing_job) }
  end
ensure
  json = JSON.pretty_generate(STATE)
  tempfile.write(json)
  tempfile.close
  FileUtils.mv(tempfile.path, STATEFILE)
  tempfile.unlink
end
