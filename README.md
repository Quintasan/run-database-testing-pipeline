# Trigger database testing pipeline job from CLI

I wrote this script because I can never find the `db:gitlabcom-database-testing` job in the dropdown.

This tool tries to be nice so it stores pipeline IDs it processed so we don't hit the pipeline jobs API endpoint unless there was a new pipeline. The "database" is stored under `~/.local/share/run-db-testing-pipeline.json`

Please let me know if you find any bugs.

## Requirement

1. Ruby
2. (Optional) 1password CLI

## Usage

Set `GITLAB_API_PRIVATE_TOKEN` environmental variable to your personal access token OR save it as `GitLab Personal Access Token` in your Personal vault in 1Password.

```
❯ run-db-testing-pipeline <MR URL>
```

Example run:

```
❯ run-db-testing-pipeline https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128496
ℹ info    Getting latest pipeline for MR !128496
• debug   latest pipeline id is 975122810
• debug   pipeline 975122810 found in the state file, job id is 4916429096
ℹ info    Database testing pipeline was already started for this pipeline
Do you want to restart the job? yes
```
